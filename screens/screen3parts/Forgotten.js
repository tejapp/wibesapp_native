import React ,{Component} from 'react';
import {Text,View,StyleSheet,FlatList,TextInput} from 'react-native';
import {Button,Header} from 'react-native-elements';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import Post from '../Post';
import ForgottenData from '../../data/ForgottenData'
export default class Forgotten extends Component {
    constructor(props){
        super(props);
       
    }

    
    render(){      
        var datas=ForgottenData;   
        return(
            <View style={styles.container_new1}>
                 <View style={styles.StylePost} >
                    <FlatList
                     data={datas}
                     renderItem={({item,Index})=>{
                            //console.log('item = ${item} , index=${inidex}')
                            return(<Post  key='${item.key}' title={item.title} dp={item.dp} occupation={item.occupation} echos={item.echos} time={item.time}
                                coins={item.coins} headline={item.headline} context={item.context}/>);
                     }}
                     _keyExtractor = {(item, index) => index.toString()}
                     //keyExtractor={(item,index) => index}
                     //_keyExtractor={(item,index)  => index.toString()}
                    >
                    </FlatList>
                </View>
            
            </View>
        );
    }
   
}
  
const styles = StyleSheet.create({
    container_new1: {
      flex: 1,
      backgroundColor:'white'
    },
    boxContainer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        
    },
    containerStyle: {
        marginTop: 75,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row'
      },
      searchTextStyle: {
        flex: 1
      },
      buttonStyle: {
        height: 30,
        marginBottom: 8
      },
      StylePost:{
          flex:1,
          flexDirection:"column"
      }
  });
  