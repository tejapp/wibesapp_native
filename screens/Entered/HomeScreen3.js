import React ,{Component} from 'react';
import {Text,View,StyleSheet,TextInput} from 'react-native';
import {Button,Header} from 'react-native-elements';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';


export default class HomeScreen3 extends Component {
    constructor(props){
        super(props);
       
    }

    render(){         
        return(
            <View style={styles.container}>
                <Text> this is home screen 3  </Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  