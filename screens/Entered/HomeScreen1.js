import React ,{Component} from 'react';
import {Text,View,StyleSheet,TextInput,FlatList} from 'react-native';
import {Button,Header,Avatar} from 'react-native-elements';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import CategoriesData from '../../data/CateogariesData';

export default class HomeScreen1 extends Component {
    constructor(props){
        super(props);
        this.state = { searchValue: null};
    }

    render(){         
        var count=0;
        data = CategoriesData;

        return (
     
    <View  style={styles.container_new1}>   
        
        <View style={styles.containerStyle}>
          <TextInput
            style={styles.searchTextStyle}
            onChangeText={searchValue => this.setState({ searchValue })}
            value={this.state.searchValue}
          />
          <Button
            buttonStyle={styles.buttonStyle}
            title="Search"
            onPress={() => console.log(this.state.searchValue)}
          />
        </View>

      <View>
            <FlatList data={CategoriesData}
            renderItem={({item,Index}) => {
               count=Index;
              //  console.log( Index );

            }}
            >
            </FlatList>
        </View>
            
        <View style={styles.boxContainer} flexDirection="row" justifyContent="space-around">
        
        <Avatar 
        source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/kfriedson/128.jpg"}}
        large rounded onPress={() => this.props.navigation.navigate('HomeScreen2')} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        
        </View>
        
        <View style={styles.boxContainer} flexDirection="row" justifyContent="space-around" >
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        </View>
        
        <View style={styles.boxContainer} flexDirection="row" justifyContent="space-around" >
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        <Avatar large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}/>
        </View>
       
    </View>

        
        );
    }
}


const styles = StyleSheet.create({
    container_new1: {
      flex: 1,
     
      
    },
    boxContainer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    containerStyle: {
        marginTop: 75,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row'
      },
      searchTextStyle: {
        flex: 1
      },
      buttonStyle: {
        height: 30,
        marginBottom: 8
      }
  });
  


