import React ,{Component} from 'react';
import {Text,View,StyleSheet,FlatList,TextInput} from 'react-native';
import {Button,Header} from 'react-native-elements';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import Post from '../Post';
import PostData from '../../data/Postdata'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HomeScreen2 extends Component {
    constructor(props){
        super(props);
       
    }

    
    render(){      
        var datas=PostData;   
        return(
            <View style={styles.container_new1}>
                <View style={styles.containerStyle}  justifyContent="space-between">
                <Button  icon={{name:'search'}}                        
                        onPress={()=> this.props.navigation.navigate('HomeScreen1')} />
                <Text style={{flex:1,textAlign:'center'}}>WibesApp </Text>
                    <Button
                        buttonStyle={styles.buttonStyle}
                        icon={{name:'new-releases'}}
                      onPress={()=> this.props.navigation.navigate('HomeScreen3')}
                    />
                    </View>
                 <View style={styles.StylePost} >
                    <FlatList
                     data={datas}
                     renderItem={({item,Index})=>{
return(<Post key={item.Key} title={item.title} dp={item.dp} occupation={item.occupation} echos={item.echos} time={item.time}
coins={item.coins} headline={item.headline} context={item.context}
  onClick={() => this.props.navigation.navigate('CompletePosts')}
/>);
                     }}
                     keyExtractor={(item,index) => item.Key}
                    >
                    </FlatList>
                </View>
            
            </View>
        );
    }
   
}
  
const styles = StyleSheet.create({
    container_new1: {
      flex: 1,
      backgroundColor:'white'
    },
    boxContainer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        
    },
    containerStyle: {
        marginTop: 75,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row'
      },
      searchTextStyle: {
        flex: 1
      },
      buttonStyle: {
        height: 30,
        marginBottom: 8
      },
      StylePost:{
          flex:1,
          flexDirection:"column"
      }
  });
  