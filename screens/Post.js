import React ,{Component} from 'react';
import {Text,View,StyleSheet,TouchableHighlight} from 'react-native';
import {Button,Avatar} from 'react-native-elements';

export default class Post extends Component{
constructor(props){
    super(props);
    this.postPressed = this.postPressed.bind(this);
}

postPressed(){
    console.log('entered into function');
    console.log(this.props.echos);
//    () => {this.props.z}
}
    render(){
        return(
    <TouchableHighlight onPress={() => this.postPressed()   } >
        <View style={styles.container} >
            
            <View style={[styles.PostBoxContainer,styles.PostBoxOne]} >
            
            <Avatar 
            source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/kfriedson/128.jpg"}}
            large rounded onPress={() => console.log("Works!")} activeOpacity={0.7}>
            
            </Avatar>
            <Text>{this.props.title} {"\n"}{"\n"}
            {this.props.occupation}  {"\n"}{"\n"}
            {this.props.headline}
            </Text>

            </View>

            <View style={[styles.PostBoxContainer,styles.PostBoxTwo]}>
                <Text> {this.props.key}</Text>
            </View>

            <View style={[styles.PostBoxContainer,styles.PostBoxThree]}  >
                <Text>{this.props.echos}</Text>
                <Text>{this.props.time}</Text>
                <Text>{this.props.coins}</Text>
            </View>
        </View>
    </TouchableHighlight>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'white'
      
    },
    PostBoxContainer:{
        flex:1,
        flexDirection:'row',       
    },
    PostBoxOne:{flex:1,alignItems:'flex-start',marginTop:40,marginLeft:20},
    PostBoxTwo:{flex:5,marginLeft:20,justifyContent:'space-around',flexDirection:'column'},
    PostBoxThree:{flex:1,alignItems:'flex-end',justifyContent:'space-around'
                    ,marginBottom:20
                }
  });
  
