import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';

export default class Invitation extends Component {
    render(){
        return(
            <View style={styles.container}>
                <Text> Humans of </Text>
                   <Text> WibesApp</Text>
                    <Text>Welcome you</Text>
                 <Button
                  backgroundColor='#000'   title="Be One Of Us !" 
                  raised icon={{name:'cached'}}
                 onPress={() => this.props.navigation.navigate('SignUpScreen')} 
                  />

                  <Button
                  backgroundColor='#000'   title="One Of Us ?" 
                  raised icon={{name:'cached'}}
                  onPress={() => this.props.navigation.navigate('LoginScreen')} 
                  />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center', 
    },
  });