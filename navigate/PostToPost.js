import React,{Component} from 'react';
import {createStackNavigator,navigationOptions} from 'react-navigation';
import {View,Text,StyleSheet} from 'react-native';
import CompletePost from '../screens/CompletePosts';

import { Header } from 'react-native-elements';



const PostToPost = new createStackNavigator({
    CompletePost:{screen:CompletePost},
    
})

export default PostToPost;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});