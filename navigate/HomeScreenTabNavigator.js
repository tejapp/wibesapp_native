import React,{Component} from 'react';
import {createBottomTabNavigator,navigationOptions} from 'react-navigation';
import {View,Text,StyleSheet} from 'react-native';
import HomeScreen1 from '../screens/Entered/HomeScreen1';
import HomeScreen2 from '../screens/Entered/HomeScreen2';
import HomeScreen3 from '../screens/Entered/HomeScreen3';
import Screen3Navigation from './Screen3Navigation';
import Screen2Navigation from './Screen2Navigation';

import { Header } from 'react-native-elements';



const HomeScreenTabNavigator = new createBottomTabNavigator({
    
    HomeScreen1:{screen:HomeScreen1},
    HomeScreen2:{screen:HomeScreen2},
    HomeScreen3:{screen:Screen3Navigation},
    
})

export default HomeScreenTabNavigator;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});