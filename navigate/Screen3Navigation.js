import React,{Component} from 'react';
import {createBottomTabNavigator,navigationOptions,createStackNavigator} from 'react-navigation';
import {View,Text,StyleSheet} from 'react-native';
import Forgotten from '../screens/screen3parts/Forgotten';
import MyWibes from '../screens/screen3parts/MyWibes';
import Memorized from '../screens/screen3parts/Memorized';

import { Header ,Button} from 'react-native-elements';


const x =new createStackNavigator({
    MyTab:{
        screen:new createBottomTabNavigator({
            MyWibes:{screen:MyWibes},
            Memorized:{screen:Memorized},
            Forgotten:{screen:Forgotten}
        }),
        navigationOptions:{ 
            header: ( /* Your custom header */
            <View
              style={{
                height: 80,
                marginTop: 23 ,
                backgroundColor:'white',
                
              }}
            >
                <View 
                style={{
                    flexDirection:'row',
                    justifyContent:'space-between',
                    marginLeft:20,
                    marginRight:20,
                    margin:10,
                }}>
                    <Button icon={{name:'arrow-back'}}/>
                    <Text>Wibrary</Text>
                    <View flexDirection='row' justifyContent='space-around'>
                        <Button icon={{name:'add'}}    />
                        <Button icon={{name:'add-shopping-cart'}}   />
                    </View>
                    
                </View>
            </View>
          ),    
        }
    }
})
const Screen3Navigation = new createBottomTabNavigator({
    MyWibes:{screen:MyWibes},
    Memorized:{screen:Memorized},
    Forgotten:{screen:Forgotten}
})

export default x;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});