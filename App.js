import React  from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator} from 'react-navigation';
import Invitation from './screens/Invitation.js'
import SignUpScreen from './screens/SignUpScreen';
import LoginScreen from './screens/LoginScreen';
import SignUpScreen2 from './screens/SignUpScreen2';
import HomeScreenTabNavigator from './navigate/HomeScreenTabNavigator';
import HomeScreen2 from './screens/Entered/HomeScreen2';
import HomeScreen3 from './screens/Entered/HomeScreen3';
import c1 from './screens/c1';
export default class App extends React.Component {
  render() {
    return (
      <AppStackNavigator/>
    );
  }
}

const AppStackNavigator = new createStackNavigator({
  c1:{screen:c1},
 Invitation:{screen:Invitation},
  LoginScreen:{screen:LoginScreen},
  SignUpScreen:{screen:SignUpScreen},
  SignUpScreen2:{screen:SignUpScreen2},
  HomeScreenTabNavigator:{screen:HomeScreenTabNavigator},
  

},{
headerMode:'none' 
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
